import java.text.NumberFormat;

public class Product implements Comparable<Product> {

    private String code;
    private String description;
    private double price;

    public Product() {
        code = "";
        description = "";
        price = 0;
    }
    
    public Product( String code, String description, double price ) {
        this.code = code;
        this.description = description;
        this.price = price;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public String getPriceFormatted() {
        NumberFormat currency = NumberFormat.getCurrencyInstance();
        return currency.format(price);
    }    
    
    @Override
    public int compareTo(Product p) {
        //Product p = (Product) o;
        // NOTE: we can use the usual relational operators  (<, >, ==, etc.)
        // when comparing numerical values. If you want to compare Strings,
        // you can use the compareTo method of the String class. See here
        // for more info: https://stackoverflow.com/a/4064770 
        /*
        if (this.getPrice() < p.getPrice()) {
            return -1;
        }
        if (this.getPrice() > p.getPrice()) {
            return 1;
        }*/
        //heres how we will implement the compareTo method
        //for products if we are comparing based on their
        //descriptions
        //(which are of the string type)
        /*System.out.println(this.getDescription().compareTo(p.getDescription()));
        if(this.getDescription().compareTo(p.getDescription()) < 0){
            
            return -1;
        } //end if

        if(this.getDescription().compareTo(p.getDescription()) > 0){
            
            return +1;
        } //end if*/

        //this bellow already gives -1,0,1. no if statement needed
        return this.getDescription().compareTo(p.getDescription());
    } // end concrete method compareTo

}
